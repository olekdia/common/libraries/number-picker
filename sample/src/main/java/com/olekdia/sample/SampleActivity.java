package com.olekdia.sample;

import android.os.Bundle;

import com.olekdia.numberpicker.MaterialNumberPicker;

import androidx.appcompat.app.AppCompatActivity;

public class SampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        final MaterialNumberPicker hourPicker = (MaterialNumberPicker) findViewById(R.id.time_value_hour_picker);
        hourPicker.setWrapSelectorWheel(true);
        hourPicker.setMinValue(0);
        hourPicker.setMaxValue(24);

        final MaterialNumberPicker minutePicker = (MaterialNumberPicker) findViewById(R.id.time_value_minute_picker);
        minutePicker.setWrapSelectorWheel(true);
        minutePicker.setMinValue(0);
        minutePicker.setMaxValue(60);
    }
}
